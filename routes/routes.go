package routes

import (
	"bitbucket.org/strom87/caloriecontrol/config"
	"bitbucket.org/strom87/caloriecontrol/controllers"
	"bitbucket.org/strom87/caloriecontrol/middlewares"
	"bitbucket.org/strom87/caloriecontrol/services"
	"github.com/gorilla/mux"
	"github.com/justinas/alice"
)

// Init initializes the routes
func Init(config config.Config, service services.Service) *mux.Router {
	router := mux.NewRouter()
	controller := controllers.New(config, service)
	middleware := middlewares.New(config, service)

	open(controller, middleware, router)
	protected(controller, middleware, router)

	return router
}

func open(c controllers.Controller, m middlewares.Middleware, r *mux.Router) {
	handler := alice.New(m.Log, m.JSON, m.MongoDb)

	r.Handle("/login", handler.ThenFunc(c.Login))
	r.Handle("/register", handler.ThenFunc(c.Register))
}

func protected(c controllers.Controller, m middlewares.Middleware, r *mux.Router) {
	handler := alice.New(m.Log, m.JSON, m.Auth, m.MongoDb)

	r.Handle("/users/{id}", handler.ThenFunc(c.UserGetOne)).Methods("GET")
	r.Handle("/foods", handler.ThenFunc(c.FoodGet)).Methods("GET")
	r.Handle("/foods/{id}", handler.ThenFunc(c.FoodOneGet)).Methods("GET")
	r.Handle("/foods", handler.ThenFunc(c.FoodPost)).Methods("POST")
	r.Handle("/foods/{id}", handler.ThenFunc(c.FoodPut)).Methods("PUT")
	r.Handle("/foods/{id}", handler.ThenFunc(c.FoodDelete)).Methods("DELETE")
}
