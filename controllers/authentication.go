package controllers

import (
	"net/http"

	"github.com/strom87/encryption"

	"bitbucket.org/strom87/caloriecontrol/database/collections"
	"bitbucket.org/strom87/caloriecontrol/models"
)

// Login used to authenticate user
func (c Controller) Login(w http.ResponseWriter, r *http.Request) {
	model := &models.Login{}
	if !c.isValidInput(w, r, model) {
		return
	}

	repository := c.repository(w, r)
	if repository == nil {
		return
	}

	user := repository.User.FindByEmail(model.Email)
	if user == nil {
		c.encodeJSON(w, models.NewResponseError(w, "User not found", http.StatusBadRequest))
		return
	}

	p := encryption.NewPasswordHash()
	ok, err := p.Match(model.Password, user.Password, user.Salt)
	if err != nil {
		c.encodeJSON(w, models.NewResponseError(w, "Internal server error, database insert", http.StatusInternalServerError))
		return
	} else if !ok {
		c.encodeJSON(w, models.NewResponseError(w, "Password not valid", http.StatusBadRequest))
		return
	}

	token, err := c.service.Jwt.GenerateToken(user)
	if err != nil {
		c.encodeJSON(w, models.NewResponseError(w, "Could not generate token", http.StatusInternalServerError))
		return
	}

	c.encodeJSON(w, models.NewResponseSuccess(w, token))
}

// Register create a new user
func (c Controller) Register(w http.ResponseWriter, r *http.Request) {
	model := &models.Register{}
	if !c.isValidInput(w, r, model) {
		return
	}

	repository := c.repository(w, r)
	if repository == nil {
		return
	}

	if exist := repository.User.EmailExist(model.Email); exist {
		c.encodeJSON(w, models.NewResponseError(w, "User exists", http.StatusConflict))
		return
	}

	password, salt, err := encryption.NewPasswordHash().Make(model.Password)
	if err != nil {
		c.encodeJSON(w, models.NewResponseError(w, "Internal server error, password generator", http.StatusInternalServerError))
		return
	}

	dbUser := collections.UserMap(model.Name, model.Email, password, salt)
	if err := repository.User.Insert(dbUser); err != nil {
		c.encodeJSON(w, models.NewResponseError(w, "Internal server error, database insert", http.StatusInternalServerError))
		return
	}

	c.encodeJSON(w, models.NewResponseSuccess(w, "User created"))
}
