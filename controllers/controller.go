package controllers

import (
	"encoding/json"
	"net/http"

	"gopkg.in/mgo.v2/bson"

	"github.com/gorilla/context"
	"github.com/gorilla/mux"
	validator "github.com/strom87/validator-go"

	"bitbucket.org/strom87/caloriecontrol/config"
	"bitbucket.org/strom87/caloriecontrol/database"
	"bitbucket.org/strom87/caloriecontrol/database/repositories"
	"bitbucket.org/strom87/caloriecontrol/models"
	"bitbucket.org/strom87/caloriecontrol/services"
)

// Controller base controller
type Controller struct {
	config  config.Config
	service services.Service
}

// New returns an instance of Controller
func New(config config.Config, service services.Service) Controller {
	return Controller{config, service}
}

func (Controller) connection(r *http.Request) *database.MongoDbConnection {
	if conn := context.Get(r, "connection"); conn != nil {
		return conn.(*database.MongoDbConnection)
	}
	return nil
}

func (c Controller) repository(w http.ResponseWriter, r *http.Request) *repositories.Repository {
	if conn := c.connection(r); conn != nil {
		return repositories.New(c.config, conn)
	}

	c.encodeJSON(w, models.NewResponseError(w, "Service unavailable", http.StatusServiceUnavailable))
	return nil
}

func (c Controller) isObjectID(v string) bool {
	return bson.IsObjectIdHex(v)
}

func (c Controller) getStringVar(r *http.Request, key string) string {
	if value, ok := mux.Vars(r)[key]; ok {
		return value
	}
	return ""
}

func (Controller) userID(r *http.Request) string {
	return context.Get(r, "id").(string)
}

func (Controller) userObjectID(r *http.Request) bson.ObjectId {
	return bson.ObjectId(context.Get(r, "id").(string))
}

func (Controller) decodeJSON(r *http.Request, v interface{}) error {
	if err := json.NewDecoder(r.Body).Decode(v); err != nil {
		return err
	}
	return nil
}

func (Controller) encodeJSON(w http.ResponseWriter, v interface{}) error {
	if err := json.NewEncoder(w).Encode(v); err != nil {
		return err
	}
	return nil
}

func (c Controller) isValidInput(w http.ResponseWriter, r *http.Request, v interface{}) bool {
	if err := c.decodeJSON(r, v); err != nil {
		c.encodeJSON(w, models.NewResponseError(w, "Input parse error, invalid input", http.StatusBadRequest))
		return false
	}

	if isValid, errorMessages, err := validator.ValidateJson(v); !isValid || err != nil {
		c.encodeJSON(w, models.NewResponseError(w, errorMessages, http.StatusBadRequest))
		return false
	}
	return true
}
