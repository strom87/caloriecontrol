package controllers

import "net/http"
import "bitbucket.org/strom87/caloriecontrol/models"

// UserGetOne get one user
func (c Controller) UserGetOne(w http.ResponseWriter, r *http.Request) {
	userID := c.getStringVar(r, "id")
	if !c.isObjectID(userID) {
		c.encodeJSON(w, models.NewResponseError(w, "User not found", http.StatusNotFound))
		return
	}

	if user := c.repository(w, r).User.FindByID(userID); user != nil {
		c.encodeJSON(w, models.NewResponseSuccess(w, user))
		return
	}

	c.encodeJSON(w, models.NewResponseError(w, "Internal server error", http.StatusInternalServerError))
}
