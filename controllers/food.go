package controllers

import (
	"net/http"

	"bitbucket.org/strom87/caloriecontrol/database/collections"
	"bitbucket.org/strom87/caloriecontrol/models"
)

// FoodGet get all foods
func (c Controller) FoodGet(w http.ResponseWriter, r *http.Request) {
	repository := c.repository(w, r)
	if repository == nil {
		return
	}

	if foods := repository.Food.All(); foods != nil {
		c.encodeJSON(w, models.NewResponseSuccess(w, foods))
		return
	}

	c.encodeJSON(w, models.NewResponseError(w, "500 internal server error", http.StatusInternalServerError))
}

// FoodOneGet get one food
func (c Controller) FoodOneGet(w http.ResponseWriter, r *http.Request) {
	foodID := c.getStringVar(r, "id")
	if !c.isObjectID(foodID) {
		c.encodeJSON(w, models.NewResponseError(w, "Food not found", http.StatusNotFound))
		return
	}

	repository := c.repository(w, r)
	if repository == nil {
		return
	}

	if food := repository.Food.FindByID(foodID); food != nil {
		c.encodeJSON(w, models.NewResponseSuccess(w, food))
		return
	}

	c.encodeJSON(w, models.NewResponseError(w, "Food not found", http.StatusNotFound))
}

// FoodPost create a new food
func (c Controller) FoodPost(w http.ResponseWriter, r *http.Request) {
	model := &models.Food{}
	if !c.isValidInput(w, r, model) {
		return
	}

	repository := c.repository(w, r)
	if repository == nil {
		return
	}

	if exist := repository.Food.NameExist(model.Name); exist {
		c.encodeJSON(w, models.NewResponseError(w, "Food already added", http.StatusConflict))
		return
	}

	food := collections.FoodMap(
		model.Name,
		model.Measurement,
		model.Amount,
		model.Calories,
	)

	if err := repository.Food.Insert(food); err != nil {
		c.encodeJSON(w, models.NewResponseError(w, "Internal server error", http.StatusBadRequest))
		return
	}

	c.encodeJSON(w, models.NewResponseSuccess(w, "Food added"))
}

// FoodPut update a food
func (c Controller) FoodPut(w http.ResponseWriter, r *http.Request) {
	foodID := c.getStringVar(r, "id")
	if !c.isObjectID(foodID) {
		c.encodeJSON(w, models.NewResponseError(w, "Food not found", http.StatusNotFound))
		return
	}

	model := &models.Food{}
	if !c.isValidInput(w, r, model) {
		return
	}

	repository := c.repository(w, r)
	if repository == nil {
		return
	}

	food := collections.FoodMap(model.Name, model.Measurement, model.Amount, model.Calories)
	if err := repository.Food.Update(foodID, food); err != nil {
		c.encodeJSON(w, models.NewResponseError(w, "Bad request", http.StatusBadRequest))
		return
	}

	c.encodeJSON(w, models.NewResponseSuccess(w, "Food updated"))
}

// FoodDelete delete a food
func (c Controller) FoodDelete(w http.ResponseWriter, r *http.Request) {
	foodID := c.getStringVar(r, "id")
	if !c.isObjectID(foodID) {
		c.encodeJSON(w, models.NewResponseError(w, "Food not found", http.StatusNotFound))
		return
	}

	repository := c.repository(w, r)
	if repository == nil {
		return
	}

	if err := repository.Food.Remove(foodID); err != nil {
		c.encodeJSON(w, models.NewResponseError(w, "Bad request", http.StatusBadRequest))
		return
	}

	c.encodeJSON(w, models.NewResponseSuccess(w, "Food removed"))
}
