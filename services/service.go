package services

import "bitbucket.org/strom87/caloriecontrol/config"

// Service wrapper for all services
type Service struct {
	Jwt JwtService
}

// New returns an instance of Service, a wrapper for all services
func New(config config.Config) Service {
	base := baseService{config}
	return Service{
		Jwt: newJwtService(base),
	}
}
