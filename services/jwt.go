package services

import (
	"crypto/rsa"
	"errors"
	"fmt"
	"io/ioutil"
	"time"

	"bitbucket.org/strom87/caloriecontrol/database/collections"
	jwt "github.com/dgrijalva/jwt-go"
)

// JwtService service for handling jwt token
type JwtService struct {
	baseService
}

func newJwtService(service baseService) JwtService {
	return JwtService{service}
}

// GenerateToken generates a new jwt token
func (j JwtService) GenerateToken(user *collections.User) (string, error) {
	token := jwt.NewWithClaims(jwt.SigningMethodRS256, jwt.MapClaims{
		"id":  user.ID.Hex(),
		"iat": time.Now().Unix(),
		"exp": time.Now().Add(time.Minute * j.config.Jwt.ExpirationMinutes).Unix(),
		"iis": "caloriecontrol.com",
	})

	key, err := j.getPrivateKey()
	if err != nil {
		return "", err
	}

	return token.SignedString(key)
}

// ValidateToken checks if the jwt token is valid
func (j JwtService) ValidateToken(tokenString string) (string, error) {
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodRSA); !ok {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}

		key, err := j.getPublicKey()
		if err != nil {
			return nil, fmt.Errorf("Invalid key %s", err)
		}

		return key, nil
	})

	if err != nil {
		return "", err
	}

	claims, ok := token.Claims.(jwt.MapClaims)
	if !ok {
		return "", errors.New("Invalid token")
	} else if !claims.VerifyExpiresAt(time.Now().Unix(), true) {
		return "", errors.New("Token expired")
	}

	id, ok := claims["id"].(string)
	if !ok {
		return "", errors.New("Claim id error")
	}

	return id, nil
}

func (j JwtService) getPublicKey() (*rsa.PublicKey, error) {
	keyBytes, err := ioutil.ReadFile(j.config.Rsa.PublicKey)
	if err != nil {
		return nil, err
	}

	return jwt.ParseRSAPublicKeyFromPEM(keyBytes)
}

func (j JwtService) getPrivateKey() (*rsa.PrivateKey, error) {
	keyBytes, err := ioutil.ReadFile(j.config.Rsa.PrivateKey)
	if err != nil {
		return nil, err
	}

	return jwt.ParseRSAPrivateKeyFromPEM(keyBytes)
}
