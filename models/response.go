package models

import "net/http"

// Response model
type Response struct {
	Data    interface{} `json:"data"`
	Code    int         `json:"code"`
	Success bool        `json:"success"`
}

// NewResponseSuccess returns an instance of a succeded response
func NewResponseSuccess(w http.ResponseWriter, data interface{}) Response {
	w.WriteHeader(http.StatusOK)

	return Response{
		Data:    data,
		Code:    http.StatusOK,
		Success: true,
	}
}

// NewResponseError returns an instance of a failed response
func NewResponseError(w http.ResponseWriter, data interface{}, code int) Response {
	w.WriteHeader(code)

	return Response{
		Data:    data,
		Code:    code,
		Success: false,
	}
}
