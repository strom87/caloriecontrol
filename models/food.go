package models

// Food model
type Food struct {
	Name        string `json:"name" validator:"required|max:50"`
	Measurement string `json:"measurement" validator:"required"`
	Amount      int    `json:"amount" validator:"required"`
	Calories    int    `json:"calories" validator:"required"`
}
