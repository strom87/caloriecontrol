package models

// Register model
type Register struct {
	Name         string `json:"name" validator:"required|max:30"`
	Email        string `json:"email" validator:"max:100|email"`
	Password     string `json:"password" validator:"min:6|max:50|equals:ConfPassword"`
	ConfPassword string `json:"conf_password"`
}
