package repositories

import (
	"bitbucket.org/strom87/caloriecontrol/config"
	"bitbucket.org/strom87/caloriecontrol/database"
	mgo "gopkg.in/mgo.v2"
)

// BaseRepository the base for a repository
type BaseRepository struct {
	config     config.Config
	connection *database.MongoDbConnection
}

func (b BaseRepository) collection(collection string) *mgo.Collection {
	return b.connection.Session.DB(b.config.MongoDb.Database).C(collection)
}
