package repositories

import (
	"bitbucket.org/strom87/caloriecontrol/database/collections"
	"bitbucket.org/strom87/caloriecontrol/helpers"
	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

// Food repository
type Food struct {
	BaseRepository
	collectionName string
}

// NewFood an instance of Food
func NewFood(b BaseRepository) Food {
	return Food{b, "foods"}
}

// Insert add a new food to collection
func (f Food) Insert(food collections.Food) error {
	return f.c().Insert(food)
}

// Update updates one record
func (f Food) Update(id string, food collections.Food) error {
	return f.c().Update(bson.M{"_id": bson.ObjectIdHex(id)}, food)
}

// Remove deletes one food record by id
func (f Food) Remove(id string) error {
	return f.c().Remove(bson.M{"_id": bson.ObjectIdHex(id)})
}

// All selects all foods from collection
func (f Food) All() []collections.Food {
	var foods []collections.Food
	if err := f.c().Find(nil).All(&foods); err != nil {
		return nil
	}
	return foods
}

// FindByID select one food by id
func (f Food) FindByID(id string) *collections.Food {
	food := collections.NewFood()
	if err := f.c().Find(bson.M{"_id": bson.ObjectIdHex(id)}).One(food); err != nil {
		return food
	}
	return nil
}

// FindByName selects one food from database by name
func (f Food) FindByName(v string) *collections.Food {
	food := collections.NewFood()
	if err := f.c().Find(bson.M{"name": v}).One(food); err != nil {
		return nil
	}
	return food
}

// NameExist returns true if user exists and false if not
func (f Food) NameExist(v string) bool {
	food := collections.NewFood()
	if err := f.c().Find(bson.M{"name_lower": helpers.CleanStringToLower(v)}).One(food); err != nil {
		return false
	}
	return true
}

func (f Food) c() *mgo.Collection {
	return f.collection(f.collectionName)
}
