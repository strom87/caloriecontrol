package repositories

import (
	"bitbucket.org/strom87/caloriecontrol/config"
	"bitbucket.org/strom87/caloriecontrol/database"
)

// Repository base repository that wraps all the repositories
type Repository struct {
	User User
	Food Food
}

// New returns an instance of Repository
func New(config config.Config, connection *database.MongoDbConnection) *Repository {
	base := BaseRepository{config, connection}

	return &Repository{
		User: NewUser(base),
		Food: NewFood(base),
	}
}
