package repositories

import (
	"bitbucket.org/strom87/caloriecontrol/database/collections"
	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

// DailyFoodConsumption repositoriy
type DailyFoodConsumption struct {
	BaseRepository
	collectionName string
}

// NewDailyFoodConsumption returns an instance of DailyFoodConsumption
func NewDailyFoodConsumption(base BaseRepository) DailyFoodConsumption {
	return DailyFoodConsumption{base, "dailyFoodConsumption"}
}

// Insert inserts one record
func (d DailyFoodConsumption) Insert(food collections.DailyFoodConsumption) error {
	return d.c().Insert(food)
}

// InsertFood inserts food for the given day
func (d DailyFoodConsumption) InsertFood(id string, item collections.DailyFoodItem) error {
	return d.c().Update(bson.M{"_id": bson.ObjectIdHex(id)}, bson.M{"$push": bson.M{"foods": item}})
}

func (d DailyFoodConsumption) c() *mgo.Collection {
	return d.collection(d.collectionName)
}
