package repositories

import (
	"log"

	"bitbucket.org/strom87/caloriecontrol/database/collections"
	"bitbucket.org/strom87/caloriecontrol/helpers"
	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

// User repository
type User struct {
	BaseRepository
	collectionName string
}

// NewUser an instance of User
func NewUser(b BaseRepository) User {
	return User{b, "users"}
}

// Insert insert a new user into the user collection
func (u User) Insert(user collections.User) error {
	return u.c().Insert(user)
}

// FindByID selects one user from database by id
func (u User) FindByID(id string) *collections.User {
	user := collections.NewUser()
	if err := u.c().Find(bson.M{"_id": bson.ObjectIdHex(id)}).One(user); err != nil {
		log.Println(id)
		return nil
	}
	return user
}

// FindByEmail selects one user from database by email
func (u User) FindByEmail(v string) *collections.User {
	user := collections.NewUser()
	if err := u.c().Find(bson.M{"email": v}).One(user); err != nil {
		return nil
	}
	return user
}

// EmailExist checks if the email already exists in the collection
func (u User) EmailExist(v string) bool {
	user := collections.NewUser()
	if err := u.c().Find(bson.M{"email": helpers.CleanStringToLower(v)}).One(user); err != nil {
		return false
	}
	return true
}

func (u User) c() *mgo.Collection {
	return u.collection(u.collectionName)
}
