package collections

import "gopkg.in/mgo.v2/bson"

// DailyFoodItem model
type DailyFoodItem struct {
	FoodID bson.ObjectId `json:"foodId" bson:"food_id"`
	Amount int           `json:"amount" bson:"amount"`
}
