package collections

import "gopkg.in/mgo.v2/bson"
import "bitbucket.org/strom87/caloriecontrol/helpers"

// User struct
type User struct {
	ID       bson.ObjectId `json:"id" bson:"_id,omitempty"`
	Name     string        `json:"name" bson:"name"`
	Email    string        `json:"-" bson:"email"`
	Password string        `json:"-" bson:"password"`
	Salt     string        `json:"-" bson:"salt"`
}

// NewUser returns an instance of User
func NewUser() *User {
	return &User{}
}

// UserMap maps values to user
func UserMap(name, email, password, salt string) User {
	return User{
		Name:     helpers.CleanString(name),
		Email:    helpers.CleanStringToLower(email),
		Password: password,
		Salt:     salt,
	}
}
