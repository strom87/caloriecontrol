package collections

import (
	"time"

	"gopkg.in/mgo.v2/bson"
)

// DailyFoodConsumption model
type DailyFoodConsumption struct {
	ID     bson.ObjectId   `json:"id" bson:"_id"`
	UserID bson.ObjectId   `json:"userId" bson:"user_id"`
	Date   time.Time       `json:"date" bson:"date"`
	Foods  []DailyFoodItem `json:"foods" bson:"foods"`
}
