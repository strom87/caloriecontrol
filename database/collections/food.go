package collections

import (
	"bitbucket.org/strom87/caloriecontrol/helpers"
	"gopkg.in/mgo.v2/bson"
)

// Food struct
type Food struct {
	ID          bson.ObjectId `json:"id" bson:"_id,omitempty"`
	Name        string        `json:"name" bson:"name"`
	NameLower   string        `json:"-" bson:"name_lower"`
	Measurement string        `json:"measurement" bson:"measurement"`
	Amount      int           `json:"amount" bson:"amount"`
	Calories    int           `json:"calories" bson:"calories"`
}

// NewFood returns an instance of dood
func NewFood() *Food {
	return &Food{}
}

// FoodMap map values to dood collection
func FoodMap(name string, measurement string, amount, calories int) Food {
	return Food{
		Name:        helpers.CleanString(name),
		NameLower:   helpers.CleanStringToLower(name),
		Measurement: helpers.CleanString(measurement),
		Amount:      amount,
		Calories:    calories,
	}
}
