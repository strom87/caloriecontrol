package main

import (
	"net/http"

	"bitbucket.org/strom87/caloriecontrol/config"
	"bitbucket.org/strom87/caloriecontrol/routes"
	"bitbucket.org/strom87/caloriecontrol/services"
)

func main() {
	config := config.New()
	service := services.New(config)
	router := routes.Init(config, service)

	http.Handle("/", router)
	http.ListenAndServe(":"+config.Port, nil)
}
