package helpers

import "strings"

// CleanString removes leading and trailing white spaces
func CleanString(v string) string {
	return strings.Trim(v, " ")
}

// CleanStringToLower removes leading and trailing white spaces and makes the string to lower
func CleanStringToLower(v string) string {
	return strings.Trim(strings.ToLower(v), " ")
}
