package middlewares

import (
	"encoding/json"
	"log"
	"net/http"
	"time"

	"github.com/gorilla/context"

	"bitbucket.org/strom87/caloriecontrol/config"
	"bitbucket.org/strom87/caloriecontrol/database"
	"bitbucket.org/strom87/caloriecontrol/models"
	"bitbucket.org/strom87/caloriecontrol/services"
)

// Middleware the different middlewares
type Middleware struct {
	config  config.Config
	service services.Service
}

// New returns an instance of Middleware
func New(config config.Config, service services.Service) Middleware {
	return Middleware{config, service}
}

// JSON set json content header
func (Middleware) JSON(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		next.ServeHTTP(w, r)
	})
}

// Log middleware for loging
func (Middleware) Log(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		startTime := time.Now()
		next.ServeHTTP(w, r)
		endTime := time.Now()
		log.Printf("[%s] %q %v\n", r.Method, r.URL.String(), endTime.Sub(startTime))
	})
}

//MongoDb middleware for mongodb connection
func (m Middleware) MongoDb(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		connection := database.NewMongoDbConnection(m.config)
		err := connection.Open()
		if err != nil {
			log.Println(err)
			json.NewEncoder(w).Encode(models.NewResponseError(w, "Database down", http.StatusInternalServerError))
			return
		}
		defer connection.Close()

		context.Set(r, "connection", connection)
		next.ServeHTTP(w, r)
	})
}

// Auth middleware for secure routes, confirms that the user has a valid token
func (m Middleware) Auth(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		id, err := m.service.Jwt.ValidateToken(r.Header.Get("Authorization"))
		if err != nil {
			log.Println(err)
			json.NewEncoder(w).Encode(models.NewResponseError(w, "401 Unauthorized", http.StatusUnauthorized))
			return
		}

		context.Set(r, "id", id)
		next.ServeHTTP(w, r)
	})
}
